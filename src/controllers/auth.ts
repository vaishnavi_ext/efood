// register login user schema forgot password verification otp
import dotenv from "dotenv";
dotenv.config();
import { Response, Request } from "express";
import * as bcrypt from "bcrypt";
import userSchema from "../models/user";
import { BCRYPT_SALT_ROUND } from "../utils/config";
const accountSid = process.env.SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);
import jwt from "jsonwebtoken";
let cloud = require("../utils/cloudinary");
import restSchema from "../models/restaurant";

export const register = async (req: Request, res: Response) => {
  try {
    const { mobileNum, emailId, password, role } = req.body;
    const findUser = await userSchema.findOne({
      $or: [
        {
          mobileNum,
        },
        {
          emailId,
        },
      ],
    });
    if (findUser) {
      throw "User already exists";
    }
    if (password.length <= 8) {
      throw "password field must be atleast 8 chars";
    }
    if (mobileNum.length < 10 || mobileNum.length > 10) {
      throw "mobileNum field must be 10 digits";
    }
    let oneTimePass = Math.floor(Math.random() * 9000 + 1000);
    let encryptedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUND);
    let data = await userSchema.create({
      ...req.body,
      password: encryptedPassword,
      confirmPassword: encryptedPassword,
      otp: oneTimePass,
    });
    await data.save();
    let number = data.mobileNum;
    client.messages
      .create({
        body: `Thank you for registering. Here is your OTP: ${oneTimePass}`,
        from: "+16696006949",
        to: "+919553250327", // number
      })
      .then((message: any) => console.log(message.sid));
    return res.status(200).json({ message: "Registration succesfull!", data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const otpVerify = async (req: Request, res: Response) => {
  try {
    const { mobileNum, otp } = req.body;
    const findUser = await userSchema.findOne({ mobileNum: mobileNum });
    if (!findUser) {
      throw "User not found";
    }
    if (findUser.otp !== otp) {
      throw "Incorrect otp";
    }
    if (findUser.otp === otp) {
      return res.status(200).json({ message: "OTP verified", findUser });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { mobileNum, password, role } = req.body;
    const findUser = await userSchema.findOne({ mobileNum: mobileNum });
    if (!findUser) {
      throw "No user found";
    }
    let pass: any = findUser.password;
    if (!(await bcrypt.compare(password, pass))) {
      return res.status(400).json({ message: "Incorrect password!" });
    }
    const token = jwt.sign(
      { mobileNum: findUser.mobileNum, role: findUser.role },
      process.env.KEY as string,
      {
        expiresIn: "1h",
      }
    );
    return res
      .status(200)
      .json({ message: "Logged in", meta: { token: token }, user: findUser });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const forgotPassword = async (req: Request, res: Response) => {
  try {
    const { mobileNum } = req.body;
    const findUser = await userSchema.findOne({ mobileNum: mobileNum });
    if (!findUser) {
      throw "No user found";
    }
    let oneTimePass = Math.floor(Math.random() * 9000 + 1000);
    let number = findUser.mobileNum;
    client.messages
      .create({
        body: `Thank you for registering. Here is your OTP: ${oneTimePass}`,
        from: "+16696006949",
        to: "+919553250327", // number
      })
      .then((message: any) => console.log(message.sid));
    await userSchema.updateOne({
      otp: oneTimePass,
    });
    return res
      .status(200)
      .json({ message: "Logged in", findUser, oneTimePass });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const changePassword = async (req: Request, res: Response) => {
  try {
    const { mobileNum, password, confirmPassword, otp } = req.body;
    const user = await userSchema.findOne({ mobileNum: mobileNum });
    if (user) {
      if (user.otp === otp) {
        const encryptedPassword = await bcrypt.hash(
          password,
          BCRYPT_SALT_ROUND
        );
        user.password = encryptedPassword;
        user.confirmPassword = encryptedPassword;
        await user.save();
        res.status(200).send({
          status: true,
          message: "Your password is updated successully.",
        });
      } else {
        throw "Otp is not valid";
      }
    } else {
      throw "User not found!";
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const updateProfile = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { password } = req.body;
    const findUser = await userSchema.findOne({ _id: id });
    if (!findUser) {
      throw "No user found";
    }
    if (req.file) {
      const result: any = await cloud.uploader.upload(req.file.path);
      findUser.image = result.secure_url;
      findUser.cloudinaryId = result.public_id;
    }
    let encryptedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUND);
    let updated = await findUser.updateOne({
      ...req.body,
      password: encryptedPassword,
      confirmPassword: encryptedPassword,
    });
    await findUser.save();
    return res.status(200).json({ message: updated, findUser });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteProfile = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const findUser = await userSchema.deleteOne({ shortId: id });
    if (findUser.deletedCount > 0) {
      return res.status(200).json({ message: "Successfully deleted!" });
    } else {
      return res.status(200).json({ message: "Employee not found!" });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
//get users
export const getUsers = async (req: Request, res: Response) => {
  try {
    const users = await userSchema.find({});
    return res.status(200).json({ message: users });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getUsersById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const users = await userSchema.find({ shortId: id });
    return res.status(200).json({ message: users });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

//adding the address
export const addressAdd = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    let { country, pincode, flat, area, landmark, town } = req.body;
    const user: any = await userSchema.findOne({ shortId: id });
    if (!user) {
      throw "No user found";
    }
    user.addresses = user.addresses.concat({
      country: country,
      pincode: pincode,
      flat: flat,
      area: area,
      landmark: landmark,
      town: town,
    });
    await user.save();
    return res
      .status(200)
      .json({ message: "Address added successfully", user });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// editing the address

export const editAddress = async (req: Request, res: Response) => {
  try {
    const { uid, aid } = req.params;
    let { country, pincode, flat, area, landmark, town } = req.body;
    const user: any = await userSchema.findOne({ shortId: uid });
    if (!user) {
      throw "No user found";
    }
    let details = user.addresses;
    for (let i = 0; i < details.length; i++) {
      if (details[i].shortId.toString() === aid) {
        details[i].country = country;
        details[i].pincode = pincode;
        details[i].flat = flat;
        details[i].area = area;
        details[i].landmark = landmark;
        details[i].town = town;
      }
    }
    await user.save();
    return res
      .status(200)
      .json({ message: "Address updated successfully", user });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

//deleting the address

export const deleteAddress = async (req: Request, res: Response) => {
  try {
    const { uid, aid } = req.params;
    const user: any = await userSchema.findOne({ shortId: uid });
    if (!user) {
      throw "No user found";
    }
    let details = user.addresses;
    for (let i = 0; i < details.length; i++) {
      if (details[i].shortId.toString() === aid) {
        details.splice(i, 1);
      }
    }
    await user.save();
    return res
      .status(200)
      .json({ message: "Address updated successfully", user });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

//add to cart

// add favs
export const addFavs = async (req: Request, res: Response) => {
  try {
    const { uid, rid, fid } = req.params;
    const { add } = req.body;
    const user = await userSchema.findOne({ _id: uid });
    if (!user) {
      throw "No user found";
    }
    const rest = await restSchema.findOne({ _id: rid });
    if (!rest) {
      throw "No restauarant found";
    }
    for (let i = 0; i < rest.foods.length; i++) {
      if (rest.foods[i]._id.toString() === fid && add === true) {
        user.favs.push(rest.foods[i]);
      } else if (rest.foods[i]._id.toString() === fid && add === false) {
        user.favs.splice(i, 1);
      }
    }
    await user.save();
    return res.status(200).json({ message: "added to fav", user });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
