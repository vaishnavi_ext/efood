import { Request, Response } from "express";
import restSchema from "../models/restaurant";
let cloud = require("../utils/cloudinary");
import userSchema from "../models/user";
import { log } from "console";
//payment

export const payments = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { paymentMode, accountHolderName, ifscCode, cardNumb, addUpi } =
      req.body;
    const details = await restSchema.findOne({ shortId: id });
    if (paymentMode === "bankAccount") {
      await restSchema.create({
        accountHolderName: accountHolderName,
        ifscCode: ifscCode,
        cardNumb: cardNumb,
      });
    }
    if (paymentMode === "upi") {
      await restSchema.create({
        addUpi: addUpi,
      });
    }
    return res.status(200).json({ message: "added payment method", details });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const createRest = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { gstinNo, longitude, latitude } = req.body;
    const user = await userSchema.findOne({ _id: id });
    const findRest: any = await restSchema.findOne({ gstinNo: gstinNo });
    if (findRest) {
      throw "Restaurant already exists";
    }
    if (!user) {
      throw "No user found";
    }
    let image: any = [];
    if (req.files) {
      let img = JSON.parse(JSON.stringify(req.files));
      for (let i = 0; i < img.length; i++) {
        let result = await cloud.uploader.upload(img[i].path);
        image.push({
          url: result.secure_url,
          cloudinaryId: result.public_id,
          index: i,
        });
      }
    }
    const newPlace: any = await restSchema.create({
      ...req.body,
      user: id,
      location: {
        type: "Point",
        coordinates: [longitude, latitude],
      },
      images: image,
    });
    await newPlace.save();
    return res.status(200).json({ message: "Restaurant added", newPlace });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getRests = async (req: Request, res: Response) => {
  try {
    const { longitude, latitude } = req.query;
    const radius = 3000;
    const restaurants = await restSchema.find({
      location: {
        $near: {
          $geometry: {
            type: "Point",
            coordinates: [longitude, latitude],
          },
          $maxDistance: radius,
        },
      },
    });
    const numRestaurants = restaurants.length;
    const location = {
      type: "Point",
      coordinates: [longitude, latitude],
    };
    return res.status(200).json({ location, numRestaurants, restaurants });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const getRestByName = async (req: Request, res: Response) => {
  try {
    let name: any = req.query.name;
    const data = await restSchema.aggregate([
      {
        $match: { address: new RegExp(name, "i") },
      },
    ]);
    return res.status(200).json({ data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const getRestsById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const rests = await restSchema.findOne({ shortId: id });
    return res.status(200).json({ restaurants: rests });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getRest = async (req: Request, res: Response) => {
  try {
    const rests = await restSchema
      .find({})
      .populate({ path: "user", select: ["name", "emailId"] })
      .populate({ path: "foods" });
    return res.status(200).json({ restaurants: rests });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const editRests = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const restOwner = await restSchema.findOne({ _id: id });
    if (!restOwner) {
      return res.status(404).json({
        success: false,
        message: "Unauthorized user!",
      });
    }
    const reqimages = req.files;

    for (let i = 0; i < 5; i++) {
      if (
        reqimages[`image${i}`] &&
        reqimages[`image${i}`][0].fieldname === `image${i}`
      ) {
        const result = await cloud.uploader.upload(
          reqimages[`image${i}`][0].path
        );
        const index = restOwner.images.find((n: any) => n.index === i);
        index.url = result.secure_url;
        index.cloudinaryId = result.public_id;
      }
    }
    let updated = await restOwner.updateOne({
      ...req.body,
    });
    await restOwner.save();
    return res.status(200).json({ message: restOwner, updated });
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const deleteRest = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const findRestToDel: any = await restSchema.deleteOne({
      shortId: id,
    });
    if (findRestToDel.deletedCount > 0) {
      return res.status(200).json({ message: "Restaurant deleted!" });
    } else {
      return res.status(404).json({ message: "Restaurant not found!" });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const addStaff = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const findRest: any = await restSchema.findOne({ shortId: id });
    if (!findRest) {
      throw " No restaurant found";
    }
    if (req.file) {
      let result = await cloud.uploader.upload(req.file.path);
      findRest.staff = findRest.staff.concat({
        ...req.body,
        image: result.secure_url,
        cloudinaryId: result.public_id,
      });
    }
    await findRest.save();
    return res.status(200).json({ message: "Restaurant added", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const editStaff = async (req: Request, res: Response) => {
  try {
    const { rid, sid } = req.params;
    const { name, position } = req.body;
    const findRest: any = await restSchema.findOne({ shortId: rid });
    if (!findRest) {
      throw " No restaurant found";
    }
    let details = findRest.staff;
    for (let i = 0; i < details.length; i++) {
      if (details[i].shortId.toString() === sid) {
        details[i].name = name;
        details[i].position = position;
        if (req.file) {
          let result = await cloud.uploader.upload(req.file.path);
          details[i].name = name;
          details[i].position = position;
          details[i].image = result.secure_url;
        }
      }
    }
    await findRest.save();
    return res.status(200).json({ message: "Restaurant added", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteStaff = async (req: Request, res: Response) => {
  try {
    const { rid, sid } = req.params;
    const findRest: any = await restSchema.findOne({ shortId: rid });
    if (!findRest) {
      throw " No restaurant found";
    }
    let details = findRest.staff;
    for (let i = 0; i < details.length; i++) {
      if (details[i].shortId.toString() === sid) {
        details.splice(i, 1);
      }
    }
    await findRest.save();
    return res.status(200).json({ message: "Restaurant added", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
