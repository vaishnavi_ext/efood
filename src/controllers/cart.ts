import { Response, Request } from "express";
import userSchema from "../models/user";
import restSchema from "../models/restaurant";
import orderSchema from "../models/orders";
import { payDetails } from "../utils/payment";
import shortid from "shortid";
export const cartFunction = async (req: Request, res: Response) => {
  try {
    const { uid, rid, fid } = req.params;
    const { add } = req.body;
    const user: any = await userSchema.findOne({ _id: uid });
    if (!user) {
      throw "No user found";
    }
    const rest: any = await restSchema
      .findOne({ _id: rid })
      .populate({ path: "foods" });
    if (!rest) {
      throw "No restauarant found";
    }
    let index = -1;
    for (let i = 0; i < rest.foods.length; i++) {
      if (rest.foods[i]._id.toString() === fid) {
        index = i;
        break;
      }
    }
    if (index !== -1) {
      if (add === true) {
        user.cart.push(rest.foods[index]);
        user.totalPrice += rest.foods[index].price;
      } else {
        user.cart.splice(index, 1);
        user.totalPrice -= rest.foods[index].price;
      }
    }
    await user.save();
    return res
      .status(200)
      .json({ message: "added to cart", total: user.totalPrice, user });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// orders
export const userOrders = async (req: Request, res: Response) => {
  try {
    const { uid, rid } = req.params;
    const { checkout } = req.body;
    const user: any = await userSchema.findOne({ _id: uid });
    const restauarant = await restSchema.findOne({ _id: rid });
    const orders = await orderSchema.find({});
    if (!user) {
      throw "No user found";
    }
    const options = {
      amount: user.totalPrice * 100,
      currency: "INR",
      receipt: shortid.generate(),
    };
    let data: any = [];
    const promises = user.cart.map(async (item: any) => {
      if (checkout === true) {
        data.push(item);
        let payment = await payDetails.orders.create(options);
        let num = Math.floor(Math.random() * 10000 + 90000);
        let orders = await orderSchema.create({
          user: uid,
          restaurant: rid,
          orders: data,
          totalPrice: user.totalPrice,
          orderId: "#ORD" + num,
          razorpayId: payment.id,
          currency: payment.currency,
        });
        user.order.push(orders);
        restauarant.order.push(orders);
        user.cart.splice(0);
      } else if (checkout === false) {
        user.cart.push(data);
      }
    });
    await Promise.all(promises);
    await user.save();
    return res.status(200).json({ message: orders });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getOrders = async (req: Request, res: Response) => {
  try {
    const orders = await orderSchema.find({}).populate({ path: "user" });
    return res.status(200).json({ orders });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const payIdStore = async (req: Request, res: Response) => {
  try {
    const { uid, oid } = req.params;
    const { payId } = req.body;
    const user: any = await userSchema.findOne({ _id: uid });
    const orders = await orderSchema.findOne({ _id: oid });
    if (orders._id.toString() === oid && orders.user.toString() === user) {
      orders.paymentId = payId;
    }
    await orders.save();
    return res.status(200).json({ message: orders });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
