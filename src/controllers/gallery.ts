import { Request, Response } from "express";
import restSchema from "../models/restaurant";
let cloud = require("../utils/cloudinary");
import mongoose from "mongoose";

export const createImage = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    let rest: any = await restSchema.findOne({ _id: id });
    if (!rest) {
      throw " No restaurant found";
    }

    let image: any = [];
    if (req.files) {
      let img = JSON.parse(JSON.stringify(req.files));
      for (let i = 0; i < img.length; i++) {
        let result = await cloud.uploader.upload(img[i].path);
        image.push({
          url: result.secure_url,
          cloudinaryId: result.public_id,
        });
      }
    }
    rest.gallery = rest.gallery.concat({
      ...req.body,
      images: image,
    });
    await rest.save();
    return res.status(200).json({ food: rest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getImages = async (req: Request, res: Response) => {
  try {
    const restaurants = await restSchema.find({});
    let images = 0;
    let filteredData: any = [];
    restaurants.forEach((rest: any) => {
      let restData: any = { name: rest.businessName, images: [] };
      rest.gallery.forEach((image: any) => {
        images++;
        restData.images.push(image);
      });
      if (restData.images.length > 0) {
        filteredData.push(restData);
      }
    });
    return res.status(200).json({ total: images, images: filteredData });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getImageById = async (req: Request, res: Response) => {
  const { rid, pid } = req.params;
  try {
    const restaurant: any = await restSchema.findOne({ _id: rid });
    const image = restaurant.gallery
      .flatMap((item: any) => item.images)
      .find((img: any) => img._id.toString() === pid);
    if (image) {
      return res.status(200).json({ images: image });
    } else {
      throw "No image found";
    }
  } catch (err) {
    return res.status(500).json({ error: err });
  }
};

export const editImage = async (req: Request, res: Response) => {
  try {
    const { id, pid } = req.params;
    const findImage: any = await restSchema.findOne({ _id: id });
    let data: any = [];
    const gallery = findImage.gallery;
    for (const item of gallery) {
      for (const image of item.images) {
        if (image._id.toString() === pid) {
          if (req.file) {
            const result = await cloud.uploader.upload(req.file.path);
            image.url = result.secure_url;
            image.cloudinaryId = result.public_id;
          }
          const doc = await mongoose
            .model("Restaurants")
            .findById(findImage._id);
          await doc.save();
          data.push(image);
        }
      }
    }
    await findImage.save();
    return res.status(200).json({ message: "Updated successfully", findImage });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteImage = async (req: Request, res: Response) => {
  try {
    const { id, pid } = req.params;
    const findImage: any = await restSchema.findOne({ _id: id });
    let data = findImage.gallery;
    data.forEach((img: any) => {
      for (let i = 0; i < img.images.length; i++) {
        if (img.images[i]._id.toString() === pid) {
          img.images.splice(1, i);
        }
      }
    });
    await findImage.save();
    return res.status(200).json({ message: "Deleted successfully", findImage });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
