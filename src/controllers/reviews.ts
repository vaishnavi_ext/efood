import restSchema from "../models/restaurant";
import reviewSchema from "../models/review";
import userSchema from "../models/user";
import { Request, Response } from "express";
let cloud = require("../utils/cloudinary");
export const createReview = async (req: Request, res: Response) => {
  try {
    const { uid, rid } = req.params;
    const user = await userSchema.findOne({ _id: uid });
    const rest = await restSchema.findOne({ _id: rid });
    const { comments, image, ratings } = req.body;
    if (!user) {
      throw "No user found";
    }
    if (!rest) {
      throw "No restaurant found";
    }
    let imgs: any = [];
    if (req.files) {
      let img = JSON.parse(JSON.stringify(req.files));
      for (let i = 0; i < img.length; i++) {
        let result = await cloud.uploader.upload(img[i].path);
        imgs.push({
          url: result.secure_url,
          cloudinaryId: result.public_id,
        });
      }
    }
    let rev = await reviewSchema.create({
      comments: comments,
      ratings: ratings,
      image: imgs,
      user: uid,
      restaurant: rid,
    });
    rest.reviews.push(rev);
    user.review.push(rev);
    await rest.save();
    await user.save();
    return res.status(200).json({ message: rev });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getReviews = async (req: Request, res: Response) => {
  try {
    const reviews = await reviewSchema
      .find({})
      .populate({ path: "restaurant", select: ["businessName"] })
      .populate({ path: "user", select: ["name", "image"] });
    return res.status(200).json({ total: reviews.length, data: reviews });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getSingleReview = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const reviews = await reviewSchema
      .findOne({ _id: id })
      .populate({ path: "restaurant", select: ["businessName"] })
      .populate({ path: "user", select: ["name", "image"] });
    return res.status(200).json({ data: reviews });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

//patch delete
export const updateReview = async (req: Request, res: Response) => {
  try {
    const { uid, rid } = req.params;
    const user: any = await userSchema.findOne({ _id: uid });
    for (let i = 0; i < user.review.length; i++) {
      if (user.review[i]._id.toString() === rid) {
      }
    }
    return res.send(user);
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteReview = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const user: any = await userSchema.findOne({ "review._id": id });
    const restaurant: any = await restSchema.findOne({ "reviews._id": id });

    user.review = user.review.filter(
      (review: any) => review._id.toString() !== id
    );
    console.log(user.review);

    restaurant.reviews = restaurant.reviews.filter(
      (review: any) => review._id.toString() !== id
    );
    console.log(restaurant.reviews);

    // console.log(restaurant.reviews);

    // const reviews = await reviewSchema.deleteOne({ _id: id });

    // if (reviews.deletedCount > 0) {
    //   return res.status(200).json({ message: "category deleted!" });
    // } else {
    //   return res.status(404).json({ message: "category not found!" });
    // }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
