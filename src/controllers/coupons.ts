import restSchema from "../models/restaurant";
import couponsSchema from "../models/coupons";
import { Request, Response } from "express";

export const createCoupon = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { offerCode, discount, offerPrice, expiryDate, description } =
      req.body;
    const restauarant = await restSchema.findOne({ _id: id });
    if (!restauarant) {
      throw "No restaurant found";
    }
    const data = await couponsSchema.create({
      offerCode: offerCode,
      discount: discount,
      offerPrice: offerPrice,
      expiryDate: expiryDate,
      description: description,
      restaurant: id,
    });
    restauarant.coupons.push(data);
    await restauarant.save();
    return res.status(200).json({ message: restauarant });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getCoupons = async (req: Request, res: Response) => {
  try {
    const copns = await couponsSchema.find({});
    return res.status(200).json({ data: copns });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const editCpn = async (req: Request, res: Response) => {
  try {
    const { rid, cid } = req.params;
    const { offerCode, discount, offerPrice, expiryDate, description } =
      req.body;
    const rest = await restSchema.findOne({ _id: rid });
    if (!rest) {
      throw " No restaurant found";
    }
    const copns = await couponsSchema.findOneAndUpdate(
      { _id: cid },
      {
        offerCode: offerCode,
        discount: discount,
        offerPrice: offerPrice,
        expiryDate: expiryDate,
        description: description,
      }
    );
    return res.status(201).json({ updated: copns });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const statusCpn = async (req: Request, res: Response) => {
  try {
    const { rid, cid } = req.params;
    const rest = await restSchema.findOne({ _id: rid });
    const { toggle } = req.body;
    if (!rest) {
      throw " No restaurant found";
    }
    const cpn = await couponsSchema.findOne({ _id: cid });
    if (toggle === true) {
      cpn.status = 1;
    } else {
      cpn.status = 0;
    }
    await cpn.save();
    return res.status(201).json({ message: cpn });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
