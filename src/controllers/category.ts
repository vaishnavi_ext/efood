import { Request, Response } from "express";
import categorySchema from "../models/category";
let cloud = require("../utils/cloudinary");

export const categoryCreate = async (req: Request, res: Response) => {
  try {
    const { title } = req.body;
    const findCat: any = await categorySchema.findOne({ title: title });
    if (findCat) {
      throw "Category already exists";
    }
    let creatCat = await categorySchema.create({ title: title });
    if (req.file) {
        let result = await cloud.uploader.upload(req.file.path);
        creatCat.url = result.secure_url
        creatCat.cloudinaryId = result.public_id
    }
    await creatCat.save();
    return res.status(200).json({ message: "Restaurant added", creatCat });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getCat = async (req: Request, res: Response) => {
  try {
    const category = await categorySchema.find({});
    return res
      .status(200)
      .json({ total: category.length, categories: category });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getCatById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const category = await categorySchema.findOne({ shortId: id });
    return res.status(200).json({ categories: category });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const editCat = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const findCatToEdit: any = await categorySchema.findOne({ shortId: id });
    let updated = await findCatToEdit.updateOne({
      ...req.body,
    });
    let images: any = [];
    if (req.files) {
      let img = JSON.parse(JSON.stringify(req.files));
      for (let i = 0; i < img.length; i++) {
        let result = await cloud.uploader.upload(img[i].path);
        images.push({
          url: result.secure_url,
          cloudinaryId: result.public_id,
        });
      }
      findCatToEdit.images = images;
    }
    await findCatToEdit.save();
    return res.status(200).json({ updated: updated, message: findCatToEdit });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteCat = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const findCatToDel: any = await categorySchema.deleteOne({
      shortId: id,
    });
    if (findCatToDel.deletedCount > 0) {
      return res.status(200).json({ message: "category deleted!" });
    } else {
      return res.status(404).json({ message: "category not found!" });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
