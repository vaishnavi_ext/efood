import express, { Request, Response } from "express";
import multer from "multer";
const router = express.Router();

import path from "path";
router.use(
  express.urlencoded({
    limit: "50mb",
    extended: true,
  })
);

router.use(
  express.json({
    limit: "50mb",
  })
);

export const upload = multer({
  storage: multer.diskStorage({}),
  fileFilter: (req: Request, file: any, cb: any) => {
    let ext = path.extname(file.originalname);
    if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
      cb(new Error("Unsupported file type!"), false);
      return;
    }
    cb(null, true);
  },
} as any);
