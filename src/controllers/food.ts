import { Request, Response } from "express";
let cloud = require("../utils/cloudinary");
import { foodSchema } from "../models/food";
import restSchema from "../models/restaurant";
export const createFood = async (req: Request, res: Response) => {
  try {
    const { rid } = req.params;
    const {
      category,
      productName,
      price,
      discountPrice,
      weight,
      packingCharge,
      description,
    } = req.body;
    // const findFood: any = await foodSchema.findOne({
    //   fid: fid,
    // });
    // if (!findFood) {
    //   throw "food Already found";
    // }
    const rest = await restSchema.findOne({ _id: rid });
    if (!rest) {
      throw "Restaurant not found";
    }
    if (!productName || !price) {
      throw "Please enter the details such as productName and price";
    }
    let image: any = [];
    if (req.files) {
      let img = JSON.parse(JSON.stringify(req.files));
      for (let i = 0; i < img.length; i++) {
        let result = await cloud.uploader.upload(img[i].path);
        image.push({
          url: result.secure_url,
          cloudinaryId: result.public_id,
        });
      }
    }
    let num = Math.floor(Math.random() * 1000 + 9000);
    let data: any = await foodSchema.create({
      ...req.body,
      category,
      productName: productName,
      price: price,
      discountPrice: discountPrice,
      weight: weight,
      packingCharge: packingCharge,
      description: description,
      fid: num,
      images: image,
      restaurant: rid,
    });
    rest.foods.push(data);
    await rest.save();
    return res.status(200).json({ restaurant: rest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const findFood = async (req: Request, res: Response) => {
  try {
    const getFood = await foodSchema.find({});
    if (!getFood) {
      throw "No foods found";
    }
    return res.status(200).json({ total: getFood.length, data: getFood });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const findFoodFilter = async (req: Request, res: Response) => {
  try {
    let queryObj: any = {};
    let name: any = req.query.name;
    if (name) {
      queryObj.foods.category.title = name;
    }
    const getFood = await foodSchema.find(queryObj);
    if (!getFood) {
      throw " No data found";
    }
    return res.status(200).json({ total: getFood });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getSingleProduct = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const getFood = await foodSchema.findOne({ fid: id });
    if (!getFood) {
      throw "No foods found";
    }
    return res.status(200).json({ data: getFood });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// export const editFood = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const findFoodToEdit: any = await foodSchema.findOne({ shortId: id });
//     if (req.file) {
//       const result = await cloud.uploader.upload(req.file.path);
//       findFoodToEdit.image = result.secure_url;
//       findFoodToEdit.cloudinary_id = result.public_id;
//     }
//     let updated = await findFoodToEdit.updateOne({
//       ...req.body,
//     });
//     await findFoodToEdit.save();
//     return res.status(200).json({ updated: updated, message: findFoodToEdit });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };
export const editFood = async (req: Request, res: Response) => {
  const { fid, pid } = req.params;
  try {
    const food: any = await foodSchema.findOne({ fid: fid });
    if (!food) {
      return res.status(404).json({ message: "Food not found" });
    }
    await food.updateOne({
      ...req.body,
    });
    food.images.forEach(async (item: any) => {
      if (item._id.toString() === pid) {
        if (req.file) {
          let result = await cloud.uploader.upload(req.file.path);
          item.url = result.secure_url;
          item.cloudinaryId = result.public_id;
        }
      }
    });
    const updatedFood = await food.save();
    res.json({ updatedFood });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Server Error" });
  }
};

export const deleteFood = async (req: Request, res: Response) => {
  try {
    const { fid } = req.params;
    const findFoodToDel: any = await foodSchema.findOneAndDelete({
      _fid: fid,
    });
    if (findFoodToDel.deletedCount > 0) {
      return res.status(200).json({ message: "category deleted!" });
    } else {
      return res.status(404).json({ message: "category not found!" });
    }
    return res
      .status(200)
      .json({ message: "deleted successfully", findFoodToDel });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// to delete images in the food
export const deleteImage = async (req: Request, res: Response) => {
  try {
    const { id, pid } = req.params;
    const findImage: any = await foodSchema.findOne({ fid: id });
    let data = findImage.gallery;
    data.forEach((img: any) => {
      for (let i = 0; i < img.images.length; i++) {
        if (img.images[i]._id.toString() === pid) {
          img.images.splice(1, i);
        }
      }
    });
    await findImage.save();
    return res.status(200).json({ message: "Deleted successfully", findImage });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// status to be active
export const statusOfDish = async (req: Request, res: Response) => {
  try {
    const { rid, fid } = req.params;
    let toggle = req.body.toggle;
    const findRest = await restSchema.findOne({ _id: rid }).populate({
      path: "foods",
    });
    const food = await foodSchema.findOne({ _id: fid });
    let data = findRest.foods;
    for (let i = 0; i < data.length; i++) {
      if (data[i]._id.toString() === fid) {
        if (toggle === true) {
          food.status = 1;
        } else {
          food.status = 0;
        }
      }
      await food.save();
    }
    return res.status(200).json({ message: "Activated", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const activateAll = async (req: Request, res: Response) => {
  try {
    const { rid } = req.params;
    let toggle = req.body.toggle;
    const findRest = await restSchema
      .findOne({ _id: rid })
      .populate({ path: "foods" });
    let data = findRest.foods;
    data.forEach(async (food: any) => {
      if (toggle === true) {
        food.status = 1;
      } else if (toggle === false) {
        food.status = 0;
      }
      await food.save();
    });
    return res.status(200).json({ message: "Activated all", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
//today's special
export const todaySpecialStatus = async (req: Request, res: Response) => {
  try {
    const { rid, fid } = req.params;
    let checkbox = req.body.checkbox;
    const findRest = await restSchema.findOne({ _id: rid });
    const food: any = await foodSchema.findOne({ _id: fid });
    let data = findRest.foods;
    for (let i = 0; i < data.length; i++) {
      if (data[i]._id.toString() === fid) {
        if (checkbox === true) {
          food.todaySpecial = true;
          findRest.bestOffers.push(food);
        } else {
          food.todaySpecial = false;
          removingTodaySpl(rid, fid);
          makingFalse(rid, fid);
        }
      }
    }
    await findRest.save();
    return res.status(200).json({ message: "Added Today special", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
const makingFalse = async (rid: any, fid: any) => {
  try {
    const findRest = await restSchema.findOne({ _id: rid });
    let todaySpl: any = findRest.todaySpl;
    for (let j = 0; j < todaySpl.length; j++) {
      if (todaySpl[j]._id.toString() === fid) {
        todaySpl[j].todaySpecial = false;
      }
    }
    await findRest.save();
  } catch (error) {
    console.log(error);
  }
};
const removingTodaySpl = async (rid: any, fid: any) => {
  try {
    const rest = await restSchema.findOne({ _id: rid });
    let tdySpl: any = rest.todaySpl;
    for (let k = 0; k < tdySpl.length; k++) {
      if (
        tdySpl[k]._id.toString() === fid &&
        tdySpl[k].todaySpecial === false
      ) {
        tdySpl.splice(k, 1);
      }
    }
    await rest.save();
  } catch (error) {
    console.log(error);
  }
};
// adding to today special
export const addingTodaySpecial = async (req: Request, res: Response) => {
  try {
    const { rid, fid } = req.params;
    const findRest = await restSchema.findOne({ _id: rid });
    let data = findRest.foods;
    data.forEach((food: any) => {
      if (food._id.toString() === fid && food.todaySpecial === true) {
        findRest.todaySpl.push(food);
      }
    });
    await findRest.save();
    return res
      .status(200)
      .json({ message: "Added to today's special", findRest });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

// removing from today's special
