import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const Coupons = new Schema(
  {
    offerCode: {
      type: String,
    },
    discount: {
      type: Number,
    },
    offerPrice: {
      type: Number,
    },
    expiryDate: {
      type: String,
    },
    description: {
      type: String,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    status: {
      type: String,
      enum: {
        values: [0, 1], // 0 inactive, 1 - active
      },
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);

Coupons.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

const couponsSchema = mongoose.model("coupons", Coupons);
export default couponsSchema;
