import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
  },
  { versionKey: false, timestamps: true }
);

const Gallery = new Schema(
  {
    images: [image],
    title: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);

Gallery.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

image.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
const gallerySchema = mongoose.model("gallery", Gallery);
export default gallerySchema;
