import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
  },
  { versionKey: false, timestamps: true }
);

const News = new Schema(
  {
    images: [image],
    blogTitle: {
      type: String,
    },
    description: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);

News.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

image.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
const latestSchema = mongoose.model("news", News);
export default latestSchema;
