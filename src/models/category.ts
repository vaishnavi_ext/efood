import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const Category = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
    title: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);

Category.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

const categorySchema = mongoose.model("category", Category);
export default categorySchema;
