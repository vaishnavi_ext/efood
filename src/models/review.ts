import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
  },
  { versionKey: false, timestamps: true }
);

const Review = new Schema(
  {
    comments: {
      type: String,
    },
    image: [image],
    ratings: {
      type: String,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);

Review.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
image.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
const reviewSchema = mongoose.model("review", Review);
export default reviewSchema;
