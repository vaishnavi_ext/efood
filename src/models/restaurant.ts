import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const Staff = new Schema(
  {
    name: {
      type: String,
    },
    position: {
      type: String,
    },
    image: {
      type: String,
    },
    cloudinaryId: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { versionKey: false, timestamps: true }
);
const orders = new Schema({
  orders: [{}],
  totalPrice: {
    type: Number,
  },
  orderDate: {
    type: Date,
    default: Date.now,
  },
  orderId: {
    type: String,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
  razorpayId: {
    type: String,
  },
  currency: {
    type: String,
  },
});
const Coupons = new Schema(
  {
    offerCode: {
      type: String,
    },
    discount: {
      type: Number,
    },
    offerPrice: {
      type: Number,
    },
    expiryDate: {
      type: String,
    },
    description: {
      type: String,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },

    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);
const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
    index: { type: Number },
  },
  { versionKey: false, timestamps: true }
);

const Gallery = new Schema(
  {
    images: [image],
    title: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);
const Review = new Schema(
  {
    comments: {
      type: String,
    },
    image: [image],
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);
const Restaurant = new Schema(
  {
    businessName: {
      type: String,
    },
    ownerName: {
      type: String,
    },
    address: {
      type: String,
    },
    gstinNo: {
      type: String,
    },
    images: [image],
    cloudinaryId: {
      type: String,
    },
    emailId: {
      type: String,
    },
    paymentMode: {
      type: String,
      enum: {
        values: ["bankAccount", "upi"],
      },
    },
    accountHolderName: {
      type: String,
    },
    ifscCode: {
      type: String,
    },
    cardNumb: {
      type: String,
    },
    addUpi: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
    location: {
      type: {
        type: String,
        enum: ["Point"],
      },
      distance: {
        type: Number,
      },
      coordinates: {
        type: [Number],
        index: "2dsphere",
        sparse: true,
      },
    },
    foods: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Food",
      },
    ],
    staff: [Staff],
    gallery: [Gallery],
    order: [orders],
    bestOffers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Food",
      },
    ],
    todaySpl: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Food",
      },
    ],
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    status: {
      type: String,
      enum: {
        values: ["open", "close"],
      },
    },
    coupons: [Coupons],
    reviews: [Review],
  },
  { versionKey: false, timestamps: true }
);

Review.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
Restaurant.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});
orders.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});
Staff.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});
Restaurant.index({ location: "2dsphere" });
Gallery.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

image.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
const restSchema = mongoose.model("Restaurants", Restaurant);

export default restSchema;
