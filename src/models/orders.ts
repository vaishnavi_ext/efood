import mongoose, { Schema, Model } from "mongoose";

const Order = new Schema(
  {
    orders: [{}],
    totalPrice: {
      type: Number,
    },
    orderDate: {
      type: Date,
      default: Date.now,
    },
    orderId: {
      type: String,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    razorpayId: {
      type: String,
    },
    paymentId: {
      type: String,
    },
    currency: {
      type: String,
    },
  },
  { versionKey: false, timestamps: true }
);

Order.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.password;
    delete ret.confirmPassword;
    delete ret.otp;
  },
});

const orderSchema = mongoose.model("Orders", Order);
export default orderSchema;
