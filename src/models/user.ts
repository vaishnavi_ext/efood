import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const address = new Schema(
  {
    country: {
      type: String,
      default: "India",
    },
    pincode: {
      type: Number,
    },
    flat: {
      type: String,
    },
    area: {
      type: String,
    },
    landmark: {
      type: String,
    },
    town: {
      type: String,
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { versionKey: false, timestamps: true }
);
const orders = new Schema({
  orders: [{}],
  totalPrice: {
    type: Number,
  },
  orderDate: {
    type: Date,
    default: Date.now,
  },
  orderId: {
    type: String,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
  razorpayId: {
    type: String,
  },
  currency: {
    type: String,
  },
});
const cart = new Schema(
  {
    food: {
      type: mongoose.Types.ObjectId,
      ref: "Food",
    },
    quantity: {
      type: Number,
      required: true,
      min: 1,
    },
  },
  { versionKey: false, timestamps: true }
);

const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
  },
  { versionKey: false, timestamps: true }
);
const Review = new Schema(
  {
    comments: {
      type: String,
    },
    image: [image],
    ratings: {
      type: String,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    shortId: { type: String, unique: true, default: shortId.generate },
  },
  { timestamps: true, versionKey: false }
);
const Food = new Schema(
  {
    category: {
      type: mongoose.Types.ObjectId,
      ref: "category",
    },
    productName: {
      type: String,
    },
    price: {
      type: Number,
    },
    discountPrice: {
      type: Number,
    },
    weight: {
      type: String,
    },
    packingCharge: {
      type: String,
    },
    description: {
      type: String,
    },
    images: [image],
    shortId: { type: String, unique: true, default: shortId.generate },
    units: {
      type: String,
    },
    fid: {
      type: String,
    },
    status: {
      type: Number,
      enum: {
        values: [0, 1], // 0- inactive, 1 active
      },
    },
    todaySpecial: {
      type: Boolean,
      enum: {
        values: [true, false],
      },
    },
    bestOffers: {
      type: Boolean,
      enum: {
        values: [true, false],
      },
    },
    fav: {
      type: Boolean,
      enum: {
        values: [true, false],
      },
    },
  },
  { versionKey: false, timestamps: true }
);
const User = new Schema(
  {
    name: {
      type: String,
    },
    mobileNum: {
      type: String,
    },
    emailId: {
      type: String,
    },
    password: {
      type: String,
    },
    confirmPassword: {
      type: String,
    },
    state: {
      type: String,
      enum: {
        values: [
          "Andhra Pradesh",
          "Arunachal Pradesh",
          "Assam",
          "Bihar",
          "Chhattisgarh",
          "Goa",
          "Gujarat",
          "Haryana",
          "Himachal Pradesh",
          "Jammu and Kashmir",
          "Jharkhand",
          "Karnataka",
          "Kerala",
          "Madhya Pradesh",
          "Maharashtra",
          "Manipur",
          "Meghalaya",
          "Mizoram",
          "Nagaland",
          "Odisha",
          "Punjab",
          "Rajasthan",
          "Sikkim",
          "Tamil Nadu",
          "Telangana",
          "Tripura",
          "Uttar Pradesh",
          "West Bengal",
        ],
      },
    },
    role: {
      type: Number,
      enum: {
        values: [0, 1, 2], //0-customer, 1 - seller, 2-admin
      },
    },
    otp: {
      type: Number,
    },
    image: {
      type: String,
    },
    cloudinaryId: {
      type: String,
    },
    order: [orders],
    favs: [Food],
    shortId: { type: String, unique: true, default: shortId.generate },
    addresses: [address],
    cart: [Food],
    review: [Review],
    totalPrice: {
      type: Number,
    },
  },
  { versionKey: false, timestamps: true }
);

User.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.password;
    delete ret.confirmPassword;
    delete ret.otp;
  },
});

address.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

orders.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});
Review.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});
const userSchema = mongoose.model("Users", User);
export default userSchema;
