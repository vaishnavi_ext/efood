import mongoose, { Schema, Model } from "mongoose";
import shortId from "shortid";

const image = new Schema(
  {
    url: { type: String },
    cloudinaryId: { type: String },
  },
  { versionKey: false, timestamps: true }
);

const Food = new Schema(
  {
    category: {
      type: mongoose.Types.ObjectId,
      ref: "category",
    },
    productName: {
      type: String,
    },
    price: {
      type: Number,
    },
    discountPrice: {
      type: Number,
    },
    weight: {
      type: String,
    },
    packingCharge: {
      type: String,
    },
    description: {
      type: String,
    },
    images: [image],
    shortId: { type: String, unique: true, default: shortId.generate },
    units: {
      type: String,
    },
    fid: {
      type: String,
    },
    status: {
      type: Number,
      enum: {
        values: [0, 1], // 0- inactive, 1 active
      },
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurants",
    },
    todaySpecial: {
      type: Boolean,
      enum: {
        values: [true, false],
      },
    },
    bestOffers: {
      type: Boolean,
      enum: {
        values: [true, false],
      },
    },
  },
  { versionKey: false, timestamps: true }
);

Food.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

image.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    ret.id = ret._id;
    delete ret._id;
  },
});

export const foodSchema = mongoose.model("Food", Food);
