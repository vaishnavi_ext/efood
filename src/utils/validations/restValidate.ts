import { NextFunction, Request, Response } from "express";

export const restCreate = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors: any = [];
  const { businessName, ownerName, address, gstinNo, emailId } = req.body;
  req.body;
  if (!businessName) {
    errors.push({ message: "businessName field is required" });
  }
  if (!ownerName) {
    errors.push({ message: "ownerName field is required" });
  }
  if (!gstinNo) {
    errors.push({ message: "gstinNo field is required" });
  }
  if (!address) {
    errors.push({ message: "address field is required" });
  }
  if (!emailId) {
    errors.push({ message: "emailId field is required" });
  }
  errors.length ? res.status(400).json({ status: "false", errors }) : next();
};
