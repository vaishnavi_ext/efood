import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import users from "../models/user";
require("dotenv").config();
const tokenValidation = async (
  permissionRole: number,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token: any = req.headers.token;
    if (!token) {
      return res.status(400).json({ message: "Token required" });
    }
    const verify: any = jwt.verify(token, process.env.KEY as string);
    const mobileNum = verify["mobileNum"];
    const user: any = await users.findOne({ mobileNum });
    req.body.currentUser = verify;
    if (permissionRole >= user.role) {
      next();
    } else {
      res.status(200).json({ message: "You cann't access this route" });
    }
  } catch (error) {
    return res.status(400).json({ message: "Invalid token", error });
  }
};

export const isCustomer = (req: Request, res: Response, next: NextFunction) =>
  tokenValidation(0, req, res, next);

export const isSeller = (req: Request, res: Response, next: NextFunction) =>
  tokenValidation(1, req, res, next);

export const isAdmin = (req: Request, res: Response, next: NextFunction) =>
  tokenValidation(2, req, res, next);

export default {
  isCustomer,
  isSeller,
};
