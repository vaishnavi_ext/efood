const express = require("express");
const router = express.Router();
import { upload } from "../controllers/upload";
import * as rest from "../controllers/restaurants";
import * as validate from "../utils/validations/restValidate";
import * as account from "../utils/tokenValidate";

router.post("/restaurants/create/:id", upload.array("image"), rest.createRest);
router.get("/restaurants", rest.getRests);
router.get("/search", rest.getRestByName);
router.get("/hotels", rest.getRest);
router.patch(
  "/restaurants-update/:id",
  account.isSeller,
  upload.fields([
    {
      name: "image0",
      maxCount: 1,
    },
    {
      name: "image1",
      maxCount: 1,
    },
    {
      name: "image2",
      maxCount: 1,
    },
    {
      name: "image3",
      maxCount: 1,
    },
    {
      name: "image4",
      maxCount: 1,
    },
  ]),
  rest.editRests
);
router.delete("/restaurants-delete/:id", account.isSeller, rest.deleteRest);
router.post("/staff/:id", upload.single("image"), rest.addStaff);
router.patch("/staff/:rid/:sid", upload.single("image"), rest.editStaff);
router.delete("/staff/:rid/:sid", rest.deleteStaff);

export default router;
