const express = require("express");
const router = express.Router();
import { upload } from "../controllers/upload";
import * as gallery from "../controllers/gallery";

router.post("/gallery/:id", upload.array("image"), gallery.createImage);
router.get("/gallery", gallery.getImages);
router.patch(
  "/gallery-update/:id/:pid",
  upload.single("image"),
  gallery.editImage
);
router.delete("/gallery-delete/:id/:pid", gallery.deleteImage);
router.get("/gallery/:rid/:pid", gallery.getImageById);

export default router;
