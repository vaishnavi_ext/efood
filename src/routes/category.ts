const express = require("express");
const router = express.Router();
import { upload } from "../controllers/upload";
import * as category from "../controllers/category";

router.post("/category", upload.single("image"), category.categoryCreate);
router.get("/categories", category.getCat);
router.patch("/category-update/:id", upload.single("image"), category.editCat);
router.delete("/category-delete/:id", category.deleteCat);
router.get("/category/:id", category.getCatById);

export default router;
