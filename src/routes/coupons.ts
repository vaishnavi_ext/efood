const express = require("express");
const router = express.Router();
import * as coupons from "../controllers/coupons";

router.post("/coupons/:id", coupons.createCoupon);
router.get("/coupons", coupons.getCoupons);
router.patch("/coupons-edit/:rid/:cid", coupons.editCpn);
router.post("/coupon-status/:rid/:cid", coupons.statusCpn);
// router.post("/payment-id/:uid/:oid", coupons.payIdStore);
// router.patch(
//   "/gallery-update/:id/:pid",
//   upload.single("image"),
//   gallery.editImage
// );
// router.delete("/gallery-delete/:id/:pid", gallery.deleteImage);
// router.get("/gallery/:rid/:pid", gallery.getImageById);

export default router;
