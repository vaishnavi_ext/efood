const express = require("express");
const router = express.Router();
import { upload } from "../controllers/upload";
import * as food from "../controllers/food";
import * as account from "../utils/tokenValidate";

router.post(
  "/food/create/:rid",
  account.isSeller,
  upload.array("image", 5),
  food.createFood
);
router.get("/foods", account.isSeller, food.findFood);
router.get("/food/:id", account.isSeller, food.getSingleProduct);
router.get("/food", account.isSeller, food.findFoodFilter);
router.patch("/food-update/:fid/:pid", upload.array("image", 5), food.editFood);
router.delete("/food-delete/:rid/:fid", food.deleteFood);
router.post("/status/:rid/:fid", food.statusOfDish);
router.post("/today-special/:rid/:fid", food.todaySpecialStatus);
router.get("/add-today-special/:rid/:fid", food.addingTodaySpecial);
router.post("/activate-all/:rid", food.activateAll);

export default router;
