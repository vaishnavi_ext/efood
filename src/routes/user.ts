const express = require("express");
const router = express.Router();
import * as auth from "../controllers/auth";
import * as validates from "../utils/validations/userValidate";
import { upload } from "../controllers/upload";

router.get("/user/:id", auth.getUsersById);
router.get("/users", auth.getUsers);
router.post("/register", validates.userValidationRegister, auth.register);
router.post("/verify", auth.otpVerify);
router.post("/login", validates.userValidationLogin, auth.login);
router.post("/forgot-password", auth.forgotPassword);
router.post("/change-password", auth.changePassword);
router.patch("/update/:id", upload.single("image"), auth.updateProfile);
router.delete("/delete/:id", auth.deleteProfile);
router.post("/address/:id", auth.addressAdd);
router.patch("/address/:uid/:aid", auth.editAddress);
router.get("/address-delete/:uid/:aid", auth.deleteAddress);
// router.post("/add-cart/:uid/:rid/:fid", auth.addToCart);
router.post("/add-fav/:uid/:rid/:fid", auth.addFavs);

export default router;
