import express from "express";
import authRoute from "./user";
import restRoute from "./restaurants";
import catRoute from "./category";
import foodRoute from "./food";
import galleryRoute from "./gallery";
import cartRoute from "./cart";
import couponRoute from "./coupons";
import revRoute from "./review";

const mainRouter = express.Router();

mainRouter.use("/account", authRoute);
mainRouter.use("/", restRoute);
mainRouter.use("/", catRoute);
mainRouter.use("/", foodRoute);
mainRouter.use("/", galleryRoute);
mainRouter.use("/", cartRoute);
mainRouter.use("/", couponRoute);
mainRouter.use("/", revRoute);

export default mainRouter;
