const express = require("express");
const router = express.Router();
import * as cart from "../controllers/cart";

router.post("/cart/:uid/:rid/:fid", cart.cartFunction);
router.post("/orders/:uid/:rid", cart.userOrders);
router.get("/orders", cart.getOrders);
router.post("/payment-id/:uid/:oid", cart.payIdStore);
// router.patch(
//   "/gallery-update/:id/:pid",
//   upload.single("image"),
//   gallery.editImage
// );
// router.delete("/gallery-delete/:id/:pid", gallery.deleteImage);
// router.get("/gallery/:rid/:pid", gallery.getImageById);

export default router;
