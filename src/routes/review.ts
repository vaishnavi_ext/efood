const express = require("express");
const router = express.Router();
import * as rev from "../controllers/reviews";
import { upload } from "../controllers/upload";

router.post("/review/:uid/:rid", upload.array("image"), rev.createReview);
router.get("/reviews", rev.getReviews);
router.patch("/review-edit/:uid/:rid", rev.updateReview);
// router.post("/coupon-status/:rid/:cid", coupons.statusCpn);
// router.post("/payment-id/:uid/:oid", coupons.payIdStore);
// router.patch(
//   "/gallery-update/:id/:pid",
//   upload.single("image"),
//   gallery.editImage
// );
router.delete("/review-delete/:id", rev.deleteReview);
router.get("/review/:id", rev.getSingleReview);

export default router;
